﻿namespace APMTestApp
{
    partial class MakeLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtEmployeePass = new System.Windows.Forms.TextBox();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.txtTransactionID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbopayment = new System.Windows.Forms.ComboBox();
            this.btnMakeLog = new System.Windows.Forms.Button();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.combLogEvent = new System.Windows.Forms.ComboBox();
            this.txtRoomNo = new System.Windows.Forms.TextBox();
            this.txtLogValue = new System.Windows.Forms.TextBox();
            this.txtRoomID = new System.Windows.Forms.TextBox();
            this.txtAccessID = new System.Windows.Forms.TextBox();
            this.txtRontID = new System.Windows.Forms.TextBox();
            this.txtReserveNumber = new System.Windows.Forms.TextBox();
            this.txtBranchID = new System.Windows.Forms.TextBox();
            this.txtLogID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLogID = new System.Windows.Forms.Label();
            this.lblResultTime = new System.Windows.Forms.Label();
            this.lblResultCode = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtEmployeePass);
            this.groupBox1.Controls.Add(this.txtEmployeeID);
            this.groupBox1.Controls.Add(this.txtTransactionID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(38, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(732, 91);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MakeLogInfo";
            // 
            // txtEmployeePass
            // 
            this.txtEmployeePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmployeePass.Location = new System.Drawing.Point(563, 32);
            this.txtEmployeePass.Name = "txtEmployeePass";
            this.txtEmployeePass.Size = new System.Drawing.Size(146, 23);
            this.txtEmployeePass.TabIndex = 3;
            this.txtEmployeePass.Text = "password";
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmployeeID.Location = new System.Drawing.Point(325, 32);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Size = new System.Drawing.Size(131, 23);
            this.txtEmployeeID.TabIndex = 2;
            this.txtEmployeeID.Text = "2000";
            // 
            // txtTransactionID
            // 
            this.txtTransactionID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTransactionID.Location = new System.Drawing.Point(103, 32);
            this.txtTransactionID.Name = "txtTransactionID";
            this.txtTransactionID.Size = new System.Drawing.Size(128, 23);
            this.txtTransactionID.TabIndex = 1;
            this.txtTransactionID.Text = "123456";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(477, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Employee Pass";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Employee ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Transaction ID";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbopayment);
            this.groupBox2.Controls.Add(this.btnMakeLog);
            this.groupBox2.Controls.Add(this.txtComment);
            this.groupBox2.Controls.Add(this.combLogEvent);
            this.groupBox2.Controls.Add(this.txtRoomNo);
            this.groupBox2.Controls.Add(this.txtLogValue);
            this.groupBox2.Controls.Add(this.txtRoomID);
            this.groupBox2.Controls.Add(this.txtAccessID);
            this.groupBox2.Controls.Add(this.txtRontID);
            this.groupBox2.Controls.Add(this.txtReserveNumber);
            this.groupBox2.Controls.Add(this.txtBranchID);
            this.groupBox2.Controls.Add(this.txtLogID);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(38, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(731, 289);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DetailInformation";
            // 
            // cbopayment
            // 
            this.cbopayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbopayment.FormattingEnabled = true;
            this.cbopayment.Items.AddRange(new object[] {
            "nopayment",
            "cash",
            "credit",
            "qr"});
            this.cbopayment.Location = new System.Drawing.Point(325, 234);
            this.cbopayment.Name = "cbopayment";
            this.cbopayment.Size = new System.Drawing.Size(134, 24);
            this.cbopayment.TabIndex = 16;
            // 
            // btnMakeLog
            // 
            this.btnMakeLog.Location = new System.Drawing.Point(537, 232);
            this.btnMakeLog.Name = "btnMakeLog";
            this.btnMakeLog.Size = new System.Drawing.Size(93, 27);
            this.btnMakeLog.TabIndex = 15;
            this.btnMakeLog.Text = "Make Log";
            this.btnMakeLog.UseVisualStyleBackColor = true;
            this.btnMakeLog.Click += new System.EventHandler(this.btnMakeLog_Click);
            // 
            // txtComment
            // 
            this.txtComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtComment.Location = new System.Drawing.Point(103, 155);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(606, 23);
            this.txtComment.TabIndex = 11;
            this.txtComment.Text = "credit,08042482466,,VISA,2017/3/29,XXXXXXXXXXXX5810,2005,3054,3,5034";
            // 
            // combLogEvent
            // 
            this.combLogEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.combLogEvent.FormattingEnabled = true;
            this.combLogEvent.Items.AddRange(new object[] {
            "checkin",
            "self checkin",
            "pre pay",
            "Checkout"});
            this.combLogEvent.Location = new System.Drawing.Point(325, 30);
            this.combLogEvent.Name = "combLogEvent";
            this.combLogEvent.Size = new System.Drawing.Size(131, 24);
            this.combLogEvent.TabIndex = 5;
            // 
            // txtRoomNo
            // 
            this.txtRoomNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRoomNo.Location = new System.Drawing.Point(325, 76);
            this.txtRoomNo.Name = "txtRoomNo";
            this.txtRoomNo.Size = new System.Drawing.Size(134, 23);
            this.txtRoomNo.TabIndex = 8;
            this.txtRoomNo.Text = "501";
            // 
            // txtLogValue
            // 
            this.txtLogValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtLogValue.Location = new System.Drawing.Point(563, 31);
            this.txtLogValue.Name = "txtLogValue";
            this.txtLogValue.Size = new System.Drawing.Size(146, 23);
            this.txtLogValue.TabIndex = 6;
            this.txtLogValue.Text = "2990";
            // 
            // txtRoomID
            // 
            this.txtRoomID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRoomID.Location = new System.Drawing.Point(563, 76);
            this.txtRoomID.Name = "txtRoomID";
            this.txtRoomID.Size = new System.Drawing.Size(146, 23);
            this.txtRoomID.TabIndex = 9;
            this.txtRoomID.Text = "22551";
            // 
            // txtAccessID
            // 
            this.txtAccessID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtAccessID.Location = new System.Drawing.Point(103, 197);
            this.txtAccessID.Name = "txtAccessID";
            this.txtAccessID.Size = new System.Drawing.Size(606, 23);
            this.txtAccessID.TabIndex = 12;
            this.txtAccessID.Text = "For Credit access_id  : 1703291457602257213681730335,1ee6852e7b84f06cd891b5daf348" +
    "9ee2,e88c93";
            // 
            // txtRontID
            // 
            this.txtRontID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRontID.Location = new System.Drawing.Point(103, 234);
            this.txtRontID.Name = "txtRontID";
            this.txtRontID.Size = new System.Drawing.Size(128, 23);
            this.txtRontID.TabIndex = 13;
            this.txtRontID.Text = "5003";
            // 
            // txtReserveNumber
            // 
            this.txtReserveNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtReserveNumber.Location = new System.Drawing.Point(103, 122);
            this.txtReserveNumber.Name = "txtReserveNumber";
            this.txtReserveNumber.Size = new System.Drawing.Size(128, 23);
            this.txtReserveNumber.TabIndex = 10;
            this.txtReserveNumber.Text = "000098";
            // 
            // txtBranchID
            // 
            this.txtBranchID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBranchID.Location = new System.Drawing.Point(103, 76);
            this.txtBranchID.Name = "txtBranchID";
            this.txtBranchID.Size = new System.Drawing.Size(128, 23);
            this.txtBranchID.TabIndex = 7;
            this.txtBranchID.Text = "1235";
            // 
            // txtLogID
            // 
            this.txtLogID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtLogID.Location = new System.Drawing.Point(103, 30);
            this.txtLogID.Name = "txtLogID";
            this.txtLogID.Size = new System.Drawing.Size(128, 23);
            this.txtLogID.TabIndex = 4;
            this.txtLogID.Text = "56347";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(237, 244);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Paymentmode";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 244);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ront ID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 204);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Access ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Comment";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Reserve Number";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(477, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Room ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(237, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Room No";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Branch ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(477, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Log Value";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Log Event";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Log ID";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblLogID);
            this.panel1.Controls.Add(this.lblResultTime);
            this.panel1.Controls.Add(this.lblResultCode);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Location = new System.Drawing.Point(38, 413);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(731, 100);
            this.panel1.TabIndex = 2;
            // 
            // lblLogID
            // 
            this.lblLogID.AutoSize = true;
            this.lblLogID.Location = new System.Drawing.Point(525, 25);
            this.lblLogID.Name = "lblLogID";
            this.lblLogID.Size = new System.Drawing.Size(0, 13);
            this.lblLogID.TabIndex = 5;
            // 
            // lblResultTime
            // 
            this.lblResultTime.AutoSize = true;
            this.lblResultTime.Location = new System.Drawing.Point(309, 25);
            this.lblResultTime.Name = "lblResultTime";
            this.lblResultTime.Size = new System.Drawing.Size(0, 13);
            this.lblResultTime.TabIndex = 4;
            // 
            // lblResultCode
            // 
            this.lblResultCode.AutoSize = true;
            this.lblResultCode.Location = new System.Drawing.Point(90, 25);
            this.lblResultCode.Name = "lblResultCode";
            this.lblResultCode.Size = new System.Drawing.Size(0, 13);
            this.lblResultCode.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(477, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Log ID:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(237, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Result Time:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Result Code:";
            // 
            // MakeLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 543);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MakeLog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MakeLog";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtEmployeePass;
        private System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.TextBox txtTransactionID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtRoomNo;
        private System.Windows.Forms.TextBox txtLogValue;
        private System.Windows.Forms.TextBox txtRoomID;
        private System.Windows.Forms.TextBox txtAccessID;
        private System.Windows.Forms.TextBox txtRontID;
        private System.Windows.Forms.TextBox txtReserveNumber;
        private System.Windows.Forms.TextBox txtBranchID;
        private System.Windows.Forms.TextBox txtLogID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.ComboBox combLogEvent;
        private System.Windows.Forms.Button btnMakeLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblResultCode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblLogID;
        private System.Windows.Forms.Label lblResultTime;
        private System.Windows.Forms.ComboBox cbopayment;
    }
}