﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APMTestApp.Models
{
    class MakeLogConditionInfo
    {
        public string transaction_id { get; set; }
        public int employee_id { get; set; }
        public string employee_pass { get; set; }
        public MakeLogRequestDetailInfo request_detail { get; set; }

    }
    public class MakeLogRequestDetailInfo
    {
        public int log_id { get; set; }
        public int log_event { get; set; }
        public int log_value { get; set; }
        public int branch_id { get; set; }
        public string room_no { get; set; }
        public int room_id { get; set; }
        public string reserve_number { get; set; }
        public string comment { get; set; }
        public string access_id { get; set; }
        public int ront_id { get; set; }
        public int paymentmode { get; set; }
    }

}
