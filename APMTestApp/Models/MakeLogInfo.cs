﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APMTestApp.Models
{
    class MakeLogInfo
    {
        public string transaction_id { get; set; }
        public string result_code { get; set; }
        public string result_time { get; set; }
        public MakeLogResultDetailInfo result_detail { get; set; }
    }
    public class MakeLogResultDetailInfo
    {
        public int log_id { get; set; }
    }
}
