﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APMTestApp.Models
{
    public class QuerySelfCheckInInfo
    {
        public string transaction_id { get; set; }
        public string result_code { get; set; }
        public string result_time { get; set; }
        public List<QuerySelfCheckInResultDetailInfo> result_detail { get; set; }
    }
    public class QuerySelfCheckInResultDetailInfo
    {
        public string reserve_number { get; set; }
        public string room_no { get; set; }
        public int room_id { get; set; }
        public string member_name { get; set; }
        public string room_type { get; set; }
        public string checkin_date { get; set; }
        public string checkout_date { get; set; }
        public int totalamount { get; set; }
        public int paid_amount { get; set; }
        public string room_option { get; set; }
        public string reduction { get; set; }
        public int balance_amount { get; set; }
        public int roomchangeflag { get; set; }
        public int noofpeople { get; set; }
        public int isforeigner { get; set; }
        public string result { get; set; }
    }
}
