﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APMTestApp.Models
{
    public class QuerySelfCheckinConditionInfo
    {
        public string transaction_id { get; set; }
        public int employee_id { get; set; }
        public string employee_pass { get; set; }
        public QuerySelfCheckinRequestDetailInfo request_detail { get; set; }
    }

    public class QuerySelfCheckinRequestDetailInfo
    {
        public string searchid { get; set; }
        public string searchtype { get; set; }
    }
}
