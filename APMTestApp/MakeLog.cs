﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using APMTestApp.Models;
using Newtonsoft.Json;
using System.Configuration;

namespace APMTestApp
{
    public partial class MakeLog : Form
    {
        string userurl = string.Empty;

        public MakeLog(string url)
        {
            InitializeComponent();
            cbopayment.SelectedItem = "nopayment";
            combLogEvent.SelectedItem = "checkin";
            userurl = url;
        }

        private async void btnMakeLog_Click(object sender, EventArgs e)
        {
            MakeLogConditionInfo cond = new MakeLogConditionInfo();
            cond.transaction_id = txtTransactionID.Text;
            cond.employee_id = int.Parse(txtEmployeeID.Text);
            cond.employee_pass = txtEmployeePass.Text;
            cond.request_detail = new MakeLogRequestDetailInfo();
            cond.request_detail.log_id = int.Parse(txtLogID.Text);
            if (combLogEvent.Text == "checkin")
                cond.request_detail.log_event = 1;
            if (combLogEvent.Text == "self checkin")
                cond.request_detail.log_event = 2;
            if (combLogEvent.Text == "pre pay")
                cond.request_detail.log_event = 3;
            if (combLogEvent.Text == "Checkout")
                cond.request_detail.log_event = 4;
            cond.request_detail.log_value = int.Parse(txtLogValue.Text);
            cond.request_detail.branch_id = int.Parse(txtBranchID.Text);
            cond.request_detail.room_no = txtRoomNo.Text;
            cond.request_detail.room_id = int.Parse(txtRoomID.Text);
            cond.request_detail.reserve_number = txtReserveNumber.Text;
            cond.request_detail.comment = txtComment.Text;
            cond.request_detail.access_id = txtAccessID.Text;
            cond.request_detail.ront_id = int.Parse(txtRontID.Text);
            if (cbopayment.Text == "nopayment")
                cond.request_detail.paymentmode = 0;
            else if (cbopayment.Text == "cash")
                cond.request_detail.paymentmode = 1;
            else if (cbopayment.Text == "credit")
                cond.request_detail.paymentmode = 2;
            else if (cbopayment.Text == "qr")
                cond.request_detail.paymentmode = 3;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(userurl);
                var jsondata = JsonConvert.SerializeObject(cond);
                var content = new StringContent(jsondata, Encoding.UTF8, "application/json");
                var result = client.PostAsync("api/MakeLog", content).Result;
                MakeLogInfo info = await result.Content.ReadAsAsync<MakeLogInfo>();
                if (info != null)
                {
                    lblResultCode.Text = info.result_code;
                    lblResultTime.Text = info.result_time;
                    lblLogID.Text = info.result_detail.log_id.ToString();
                }
            }
        }

       
    }
}
