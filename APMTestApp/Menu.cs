﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using APMTestApp.Models;
using System.Configuration;

namespace APMTestApp
{
    public partial class Menu : Form
    {
        string url = ConfigurationManager.AppSettings["urlpath"];
        public static string apiurl = string.Empty;
        public Menu()
        {
            InitializeComponent();
        }

        private void btnQuerySelfCheckin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtURL.Text))
                apiurl = txtURL.Text;
            else
                apiurl = url;
            var form = new QuerySelfCheckin(apiurl);
            form.Show();
        }

        private void btnlog_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtURL.Text))
                apiurl = txtURL.Text;
            else
                apiurl = url;
            var form = new MakeLog(apiurl);
            form.Show();
        }
    }
}
