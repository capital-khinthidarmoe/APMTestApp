﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using APMTestApp.Models;
using Newtonsoft.Json;
using System.Net.Security;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;

namespace APMTestApp
{
    
    public partial class QuerySelfCheckin : Form
    {
        string userurl = string.Empty;

        public QuerySelfCheckin(string url)
        {
            InitializeComponent();
            cboSearchType.SelectedItem = "phonenumber";
            userurl = url;
            //txtUrl.Text = url;
        }

        private async void btnSelfCheckin_Click(object sender, EventArgs e)
        {
            DataTable dt = CreateTable();
            QuerySelfCheckinConditionInfo cond = new QuerySelfCheckinConditionInfo();
            cond.transaction_id = txtTransID.Text;
            cond.employee_id = int.Parse(txtEmpID.Text);
            cond.employee_pass = txtEmpPass.Text;
            cond.request_detail = new QuerySelfCheckinRequestDetailInfo();
            cond.request_detail.searchid = txtSearchID.Text;
            if (cboSearchType.Text == "phonenumber")
                cond.request_detail.searchtype = "1";
            else if (cboSearchType.Text == "reservenumber")
                cond.request_detail.searchtype = "2";
            else if (cboSearchType.Text == "memberId")
                cond.request_detail.searchtype = "3";
           
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(userurl);
                var jsondata = JsonConvert.SerializeObject(cond);
                var content = new StringContent(jsondata, Encoding.UTF8, "application/json");
                var result = client.PostAsync("api/QuerySelfCheckin", content).Result;
                QuerySelfCheckInInfo info = await result.Content.ReadAsAsync<QuerySelfCheckInInfo>();
                if (info!=null)
                {
                    lblResultCode.Text = info.result_code;
                    lblResultTime.Text = info.result_time;
                    for (int i = 0; i < info.result_detail.Count; i++)
                    {
                        dt.Rows.Add();
                        dt.Rows[i]["Reserve No"] = info.result_detail[i].reserve_number;
                        dt.Rows[i]["Room No"] = info.result_detail[i].room_no;
                        dt.Rows[i]["Room ID"] = info.result_detail[i].room_id;
                        dt.Rows[i]["Member Name"] = info.result_detail[i].member_name;
                        dt.Rows[i]["Room Type"] = info.result_detail[i].room_type;
                        dt.Rows[i]["Checkin Date"] = info.result_detail[i].checkin_date;
                        dt.Rows[i]["Checkout Date"] = info.result_detail[i].checkout_date;
                        dt.Rows[i]["Total Amount"] = info.result_detail[i].totalamount;
                        dt.Rows[i]["Paid Amount"] = info.result_detail[i].paid_amount;
                        dt.Rows[i]["Reduction"] = info.result_detail[i].reduction;
                        dt.Rows[i]["Balance Amount"] = info.result_detail[i].balance_amount;
                        dt.Rows[i]["Room Change Flag"] = info.result_detail[i].roomchangeflag;
                        dt.Rows[i]["No. of People"] = info.result_detail[i].noofpeople;
                        dt.Rows[i]["Is Foreigner"] = info.result_detail[i].isforeigner;
                        dt.Rows[i]["Result"] = info.result_detail[i].result;
                    }
                    grdQuerySelfCheckin.DataSource = dt;
                }
            }
        }

        private DataTable CreateTable()
        {
            #region Create table
            DataTable dt = new DataTable();
            dt.Columns.Add("Reserve No", typeof(string));
            dt.Columns.Add("Room No", typeof(string));
            dt.Columns.Add("Room ID", typeof(int));
            dt.Columns.Add("Member Name", typeof(string));
            dt.Columns.Add("Room Type", typeof(string));
            dt.Columns.Add("Checkin Date", typeof(string));
            dt.Columns.Add("Checkout Date", typeof(string));
            dt.Columns.Add("Total Amount", typeof(int));
            dt.Columns.Add("Paid Amount", typeof(int));
            dt.Columns.Add("Room Option", typeof(string));
            dt.Columns.Add("Reduction", typeof(string));
            dt.Columns.Add("Balance Amount", typeof(int));
            dt.Columns.Add("Room Change Flag", typeof(int));
            dt.Columns.Add("No. of People", typeof(int));
            dt.Columns.Add("Is Foreigner", typeof(int));
            dt.Columns.Add("Result", typeof(string));
            #endregion
            return dt;
        }

        private void txtEmpID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

       
    }
}
